import React from 'react';

function ActBotton (props){
    return(
    <div className="actBtn">
        <a href="#" className='actBtn-link'>{props.text}</a>
    </div>
    );
}

export default ActBotton;